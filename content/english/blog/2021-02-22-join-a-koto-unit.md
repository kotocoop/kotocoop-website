---
layout: post
title: 'Join a Koto unit!'
sub_heading: ''
date: 2021-02-22 14:30:00 +0000
tags: []
images:
- /img/blog/join-a-unit.jpg

related_posts: []

---

We are looking for people who are interested in joining a unit in Koto. Everybody interested should fill this [questionnaire](https://kotocoop.org/joining_a_unit).

Currently we are mostly working in Europe, but if you are interested and living elsewhere, we would like to hear about you too.

The first unit will be likely be a small place in the finnish countryside with accommodation, a workshop and land to test out different agricultural methods. The details largely depend on what people joining are interested in and want to make out of it, so it is important that we get answers even if people are unsure of what kind of a unit they would like to join.

