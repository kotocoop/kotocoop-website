---
layout: post
title: 'A new article about Koto-model'
sub_heading: ''
date: 2023-11-18T12:50:00.0000
tags: []
images:
- /img/blog/default.png
related_posts: []

---

We updated the website content to hopefully better explain what Koto co-op is. You can find it at [here](/model/).
