---
layout: post
title: 'Koto Co-op code of conduct'
sub_heading: ''
date: 2021-08-21 12:30:00 +0000
tags: []
images:
- /img/blog/code_of_conduct.jpg

related_posts: []

---

We have added [a code of conduct](/code_of_conduct) for the Koto Co-op organization. It applies in internal and external Koto Co-op activities such as internal messaging, activities and other Co-op related operations. As the community around Koto Co-op is growing all the time, it was mandatory to create one to ensure equal treatment for everybody working and participating in our activities. 

Code of conduct consists of different guidelines on how to be respectful and humane toward others. We wish that everyone participating in Koto activities would take a little time of their lives and read through the code of conduct. If you have any questions regarding the code of conduct, feel free to contact us and ask more questions. Thank you for your time!

[Koto code of conduct](/code_of_conduct)